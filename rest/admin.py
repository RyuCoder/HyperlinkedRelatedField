from django.contrib import admin

from rest.models import Album, Track

admin.site.register(Album)
admin.site.register(Track)
