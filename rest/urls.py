from django.conf.urls import url
from rest import views


urlpatterns = [
    url(r'^$', views.IndexAPIView.as_view(), name="index"),
    url(r'^user$', views.UserAPIView.as_view(), name="user"),
    url(r'^album/detail/(?P<pk>\d+)/$', views.AlbumDetailAPIView.as_view(), name="album-detail"),
]
