from rest_framework.generics import (CreateAPIView, ListAPIView,
                                     RetrieveAPIView, UpdateAPIView, DestroyAPIView)

from rest import models
from rest.serializers import AlbumSerializer, UserSerializer
from django.contrib.auth.models import User


class IndexAPIView(ListAPIView):
    queryset = models.Album.objects.all()
    serializer_class = AlbumSerializer


class UserAPIView(ListAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class AlbumDetailAPIView(RetrieveAPIView):
    queryset = models.Album.objects.all()
    serializer_class = AlbumSerializer
