from django.contrib.auth.models import User
from rest_framework import serializers

from rest import models


class UserSerializer(serializers.ModelSerializer):
    # albums = serializers.StringRelatedField(many=True)
    # albums = serializers.PrimaryKeyRelatedField(many=True, read_only=True)
    albums = serializers.HyperlinkedRelatedField(
        many=True,
        read_only=True,
        view_name='rest:album-detail',
        # lookup_field='pk'
    )

    class Meta:
        model = User
        fields = ['id', 'username', 'albums']

class AlbumSerializer(serializers.ModelSerializer):
    tracks = serializers.StringRelatedField(many=True)

    class Meta:
        model = models.Album
        fields = ('album_name', 'artist', 'tracks')
